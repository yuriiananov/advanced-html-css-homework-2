"use strict";

const button = document.querySelector('.header__nav-btn');
const menu = document.querySelector('.header__nav-list');
const links = document.querySelectorAll('.menu__item');

window.addEventListener('resize', function () {
    if (document.body.clientWidth >= 768) {
        menu.classList.remove('show');
        button.classList.remove('active');
    }
});

button.addEventListener('click', () => {
    button.classList.toggle('active');
    menu.classList.toggle('show');
    links.forEach(link => {
        link.classList.toggle('show');
    });
});

